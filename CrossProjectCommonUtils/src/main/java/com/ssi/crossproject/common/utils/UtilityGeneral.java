package com.ssi.crossproject.common.utils;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;

import org.apache.commons.lang3.builder.HashCodeBuilder;
import org.springframework.stereotype.Service;

public class UtilityGeneral {
	public static StringBuilder getConcatSb(Object... vals){
		StringBuilder sb = new StringBuilder();
		if(vals != null){
			for(int i=0; i<vals.length; i++){
				Object val = vals[i];
				String strVal = null;
				if(val != null){
					if(val instanceof String){
						strVal = (String)val;
					}
					else if(val instanceof Integer){
						strVal = String.valueOf((Integer)val);
					}
					else if(val instanceof Double){
						strVal = String.valueOf((Double)val);
					}
					else{
						strVal = val.toString();
					}
				}
				sb.append(strVal);
			}
		}
		return sb;
	}
	public static String concat(Object... objs){
		StringBuilder sb = getConcatSb(objs);
		return sb.toString();
	}
	
	public static String getLineSeperator(){
		return System.lineSeparator();
	}
	
	/**
	 * replaces multiple whitespace characters (including space, tab, etc.) with single space
	 * @param originalString
	 * @return String
	 */
	public static String replaceContinuousSpacesWithSingleSpace(String originalString){
		if(originalString != null){
			return originalString.replaceAll("[^\\S\\r\\n]+", " ");
		}
		return originalString;
	}
	public static boolean isEmpty(String str){
		if(str == null || "".equals(str.trim())){
			return true;
		}
		return false;
	}
	
	/**
     * Queries {@code tasklist} if the process ID {@code pid} is running.
     * @param pid the PID to check
     * @return {@code true} if the PID is running, {@code false} otherwise
     */
    public static boolean isProcessIdRunningOnWindows(int pid){
        try {
            Runtime runtime = Runtime.getRuntime();
            String cmds[] = {"cmd", "/c", "tasklist /FI \"PID eq " + pid + "\""};
            Process proc = runtime.exec(cmds);

            InputStream inputstream = proc.getInputStream();
            InputStreamReader inputstreamreader = new InputStreamReader(inputstream);
            BufferedReader bufferedreader = new BufferedReader(inputstreamreader);
            String line;
            while ((line = bufferedreader.readLine()) != null) {
                //Search the PID matched lines single line for the sequence: " 1300 "
                //if you find it, then the PID is still running.
                if (line.contains(" " + pid + " ")){
                    return true;
                }
            }

            return false;
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("Cannot query the tasklist for some reason.");
//            System.exit(0);
        }

        return false;

    }
    
    public static String getStackStraceAsString(Exception e){
		if(e != null){
			StringWriter sw = new StringWriter();
			e.printStackTrace(new PrintWriter(sw));
			return sw.toString();
		}		
		return "";
	}
    
    public static int generateHashCode(Object... obj){
    	HashCodeBuilder hcb = new HashCodeBuilder(17, 37);
    	if(obj != null && obj.length>0){
    		for(int i=0; i<obj.length; i++){
    			hcb.append(obj[i]);
    		}
    	}
        return hcb.toHashCode();
    }
}
