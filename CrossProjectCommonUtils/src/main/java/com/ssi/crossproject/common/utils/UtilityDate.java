package com.ssi.crossproject.common.utils;

import java.util.Date;





//import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.DateTimeFieldType;
import org.joda.time.DateTimeZone;
import org.joda.time.LocalTime;
import org.joda.time.format.DateTimeFormat;
import org.joda.time.format.DateTimeFormatter;
import org.joda.time.format.DateTimeFormatterBuilder;
import org.joda.time.format.DateTimeParser;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;



public  class UtilityDate {
//	private static final Logger logger = LoggerFactory.getLogger(UtilityDate.class);
	public static final String F_YYYY_MM_DD = "yyyy-MM-dd";
	public static final String F_YYYYMM = "yyyyMM";
	public static final String F_YYYYMMDD = "yyyyMMdd";
	public static final String F_YYYY_MM_DD_HH_MM_SS = "yyyy-MM-dd HH:mm:ss";
	public static final String F_MYSQL_TIMESTAMP  = "yyyy-MM-dd HH:mm:ss";
	public static final String F_MYSQL_DATETIME   = "yyyy-MM-dd HH:mm:ss";
	public static final String F_MYSQL_DATE   = "yyyy-MM-dd";
	public static final String F_MYSQL_YEAR_YYYY   = "yyyy";
	public static final String F_MYSQL_YEAR_YY   = "yy";
	public static final String F_HH_mm_ss = "HH:mm:ss";
	public static final String F_hh_mm_ss_a = "hh:mm:ss a";
	public static final String F_DD_MM_HH_mm_ss = "dd-MM HH:mm:ss";
	public static final String F_DATETIME_FOR_FILE_NAME = "MM-dd-yyyy-HH-mm-ss";
	public static final String F_DD_MM_hh_mm_ss_a = "dd-MM hh:mm:ss a";
	public static final String F_MM_dd_HH_mm_ss = "MM-dd HH:mm:ss";
	
	public static DateTime toDateTime(String date, String format){
        DateTime dateTime = DateTime.parse(date, DateTimeFormat.forPattern(format));
        return dateTime;
    }
	
	public static String getStringDate(DateTime dateTime, String format){			
		// Format for output
		DateTimeFormatter dtfOut = DateTimeFormat.forPattern(format);		
		return dtfOut.print(dateTime);
	}
	
	public static String getStringDate(Date date, String format){	
		if(date != null){
			DateTime dateTime = new DateTime(date);
			// Format for output
			DateTimeFormatter dtfOut = DateTimeFormat.forPattern(format);		
			return dtfOut.print(dateTime);
		}
		return "";
	}
	
	public static LocalTime getOnlyTime(){
		return LocalTime.now();
	}
	/**
	 * If current time is less than given time than previous day is prefixed else current day is prefixed else . 
	 * @param time
	 * @return
	 */
	public static String addDayPart(String time){ 
		String[] formats = new String[]{F_HH_mm_ss, F_hh_mm_ss_a};
		if(formats != null && formats.length>0 && time != null){
			try{
				int len = formats.length;
				DateTimeParser[] parsers = new DateTimeParser[len];
					
				for(int i=0; i<len; i++){
					parsers[i] = DateTimeFormat.forPattern( formats[i] ).getParser();
				}				
				DateTimeFormatter formatter = new DateTimeFormatterBuilder().append( null, parsers ).toFormatter();
				
				
				LocalTime givenTime = LocalTime.parse(time, formatter);
				DateTime currDT = DateTime.now();
				LocalTime currTime = LocalTime.parse(currDT.toString(F_HH_mm_ss), DateTimeFormat.forPattern( F_HH_mm_ss ));
				if(currTime.isBefore(givenTime)){
					//this is a border line case where next date is just started
					currDT = currDT.minusDays(1);
				}
				return UtilityGeneral.concat(currDT.toString(F_YYYY_MM_DD), " ", time);
			}
			catch(Exception e){
				e.printStackTrace();
				//logger.error(UtilityGeneral.concat("Exception when adding date to time = ", timestamp),e);
			}
		}
		return "";
	}
	
	/**
	 * If current time is less than given time than previous day is prefixed else current day is prefixed else . 
	 * @param dateTime
	 * @return
	 */
	public static String addYearPart(String dateTime){ 
		String[] formats = new String[]{F_DD_MM_HH_mm_ss, F_DD_MM_hh_mm_ss_a};
		if(formats != null && formats.length>0 && dateTime != null){
			try{
				int len = formats.length;
				DateTimeParser[] parsers = new DateTimeParser[len];
					
				for(int i=0; i<len; i++){
					parsers[i] = DateTimeFormat.forPattern( formats[i] ).getParser();
				}				
				DateTimeFormatter formatter = new DateTimeFormatterBuilder().append( null, parsers ).toFormatter();
				
				
				DateTime givenDateTime = DateTime.parse(dateTime, formatter);
				int givenMonth = givenDateTime.getMonthOfYear();
				DateTime currDT = DateTime.now();
				int currMonth = currDT.getMonthOfYear();
				int currYear = currDT.getYear();
				LocalTime currTime = LocalTime.parse(currDT.toString(F_HH_mm_ss), DateTimeFormat.forPattern( F_HH_mm_ss ));
				if(currMonth < givenMonth){
					//this is a border line case where next year is just started
					currYear--;
				}
				return UtilityGeneral.concat(String.valueOf(currYear), "-", givenDateTime.toString(F_MM_dd_HH_mm_ss));
			}
			catch(Exception e){
				e.printStackTrace();
				//logger.error(UtilityGeneral.concat("Exception when adding date to time = ", timestamp),e);
			}
		}
		return "";
	}
	
	public  static String getDate(String strDateTime){		
		// Format for input
		DateTimeFormatter dtf = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		// Parsing the date
		DateTime jodatime = dtf.parseDateTime(strDateTime);
		// Format for output
		DateTimeFormatter dtfOut = DateTimeFormat.forPattern("yyyy-MM-dd");
		
		return dtfOut.print(jodatime);
	}
	public static String getCurrentUtiDate(){	
		return getCurrentUtiDate("yyyy-MM-dd HH:mm:ss");
	}
	public static String convertLocalToUTC(String inLocalDateTime, String inDateFormat, String outDateFormat){
		try{
			String[] arr = new String[]{inDateFormat};
			DateTime inDt = getDateTimeFromStringByMultiFormats(inLocalDateTime, arr);
			DateTime outDate = inDt.toDateTime(DateTimeZone.UTC);
			return getStringDate(outDate, outDateFormat);
		}
		catch(Exception e){
			
		}
		return inLocalDateTime;
	}
	public static String getCurrentUtiDate(String format){			
		// Format 
		DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
		// Parsing the date
		DateTime jodatime = DateTime.now(DateTimeZone.UTC);		
		return dtf.print(jodatime);
	}
	public static String getCurrentLocalDate(String format){			
		// Format 
		DateTimeFormatter dtf = DateTimeFormat.forPattern(format);
		// Parsing the date
		DateTime jodatime = DateTime.now();		
		return dtf.print(jodatime);
	}
	public static String getDateMultiFormats(String strDateTime){		
		DateTimeParser[] parsers = { 
		        DateTimeFormat.forPattern( "yyyy-MM-dd HH:mm:ss" ).getParser(),
		        DateTimeFormat.forPattern( "yyyy-MM-dd HH:mm:ss.SSS" ).getParser() };
		DateTimeFormatter formatter = new DateTimeFormatterBuilder().append( null, parsers ).toFormatter();

		DateTime jodatime = formatter.parseDateTime(strDateTime);
		// Format for output
		DateTimeFormatter dtfOut = DateTimeFormat.forPattern("yyyy-MM-dd");
		
		return dtfOut.print(jodatime);
	}
	public static DateTime getDateTimeFromStringByMultiFormats(String strDateTime, String... formats){		
		if(formats != null && formats.length>0){
			DateTimeParser[] parsers = new DateTimeParser[formats.length];
			for(int i=0; i<formats.length; i++){
				parsers[i] = DateTimeFormat.forPattern( formats[i]).getParser();
			}
			DateTimeFormatter formatter = new DateTimeFormatterBuilder().append( null, parsers ).toFormatter();
			return formatter.parseDateTime(strDateTime);
		}
		else{
			return null;
		}
	}
	public static Date getDateFromStringByMultiFormats(String strDateTime, String... formats){	
		DateTime dt = getDateTimeFromStringByMultiFormats(strDateTime, formats);
		if(dt != null){
			return dt.toDate();
		}
		else{
			return null;
		}
	}
	
	public static String converMillisecondsToString(long mili){
		// Parsing the date
		DateTime jodatime = new DateTime(mili);
		// Format for output
		DateTimeFormatter dtfOut = DateTimeFormat.forPattern("yyyy-MM-dd HH:mm:ss");
		
		return dtfOut.print(jodatime);
	}
	
//	public static void main(String[] arg){
//		System.out.println(addDayPart("2:00:13 AM"));
//	}
	
}
