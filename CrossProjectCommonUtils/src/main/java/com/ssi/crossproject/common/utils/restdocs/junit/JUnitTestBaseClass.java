package com.ssi.crossproject.common.utils.restdocs.junit;



import org.springframework.cloud.sleuth.instrument.web.TraceFilter;
import org.springframework.core.env.Environment;

import static capital.scalable.restdocs.AutoDocumentation.authorization;
import static capital.scalable.restdocs.AutoDocumentation.description;
import static capital.scalable.restdocs.AutoDocumentation.methodAndPath;
import static capital.scalable.restdocs.AutoDocumentation.pathParameters;
import static capital.scalable.restdocs.AutoDocumentation.requestFields;
import static capital.scalable.restdocs.AutoDocumentation.requestParameters;
import static capital.scalable.restdocs.AutoDocumentation.responseFields;
import static capital.scalable.restdocs.AutoDocumentation.section;
import static capital.scalable.restdocs.jackson.JacksonResultHandlers.prepareJackson;
import static capital.scalable.restdocs.response.ResponseModifyingPreprocessors.limitJsonArrayLength;
import static capital.scalable.restdocs.response.ResponseModifyingPreprocessors.replaceBinaryContent;
import static org.springframework.restdocs.cli.CliDocumentation.curlRequest;
import static org.springframework.restdocs.http.HttpDocumentation.httpRequest;
import static org.springframework.restdocs.http.HttpDocumentation.httpResponse;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.boot.test.web.client.TestRestTemplate;
import org.junit.Before;
import org.junit.Rule;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.restdocs.JUnitRestDocumentation;
import org.springframework.restdocs.mockmvc.RestDocumentationResultHandler;
import org.springframework.restdocs.operation.preprocess.OperationResponsePreprocessor;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.setup.DefaultMockMvcBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.context.WebApplicationContext;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.ssi.crossproject.common.utils.UtilityGeneral;

import static capital.scalable.restdocs.AutoDocumentation.authorization;
import static capital.scalable.restdocs.AutoDocumentation.description;
import static capital.scalable.restdocs.AutoDocumentation.methodAndPath;
import static capital.scalable.restdocs.AutoDocumentation.pathParameters;
import static capital.scalable.restdocs.AutoDocumentation.requestFields;
import static capital.scalable.restdocs.AutoDocumentation.requestParameters;
import static capital.scalable.restdocs.AutoDocumentation.responseFields;
import static capital.scalable.restdocs.AutoDocumentation.section;
import static capital.scalable.restdocs.jackson.JacksonResultHandlers.prepareJackson;
import static capital.scalable.restdocs.misc.AuthorizationSnippet.documentAuthorization;
import static capital.scalable.restdocs.response.ResponseModifyingPreprocessors
        .limitJsonArrayLength;
import static capital.scalable.restdocs.response.ResponseModifyingPreprocessors
        .replaceBinaryContent;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.greaterThan;
import static org.hamcrest.Matchers.is;
import static org.hamcrest.Matchers.notNullValue;
import static org.springframework.restdocs.cli.CliDocumentation.curlRequest;
import static org.springframework.restdocs.http.HttpDocumentation.httpRequest;
import static org.springframework.restdocs.http.HttpDocumentation.httpResponse;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.document;
import static org.springframework.restdocs.mockmvc.MockMvcRestDocumentation.documentationConfiguration;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessRequest;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.preprocessResponse;
import static org.springframework.restdocs.operation.preprocess.Preprocessors.prettyPrint;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment=WebEnvironment.RANDOM_PORT)
public class JUnitTestBaseClass {
	@LocalServerPort
    protected int randomServerPort;

	@Autowired
	protected Environment environment;

	protected String port = null;
	protected String serverPath = null;
//    @LocalManagementPort
//    int randomManagementPort;
    
	private static final String DEFAULT_AUTHORIZATION = "Resource is public.";
	
	protected String  schemeForDocs = null;
	protected String  hostForDocs = null;
    protected Integer portForDocs = null;
    
	private String  schemeForDocsDefault = "http";
	private String  hostForDocsDefault = "localhost";
	private Integer portForDocsDefault = 8080;

    @Autowired
    protected WebApplicationContext context;

    @Autowired
    protected ObjectMapper objectMapper;

    protected MockMvc mockMvc;
    protected MockMvc mockMvcWithoutDoc;
//    protected MockMvc mockMvcWithoutDoc;
//    protected RestTemplate restTemplate = new RestTemplate();
    
    @Autowired
    protected TestRestTemplate testRestTemplate;
    
//    @InjectMocks
//    TraceFilter traceFilter;
  

    
    @Rule
    public final JUnitRestDocumentation restDocumentation =
            new JUnitRestDocumentation(resolveOutputDir());

    private String resolveOutputDir() {
        String outputDir = System.getProperties().getProperty(
                "org.springframework.restdocs.outputDir");
        if (outputDir == null) {
            outputDir = "target/generated-snippets";
        }
        return outputDir;
    }
    
    private void customisation(){
    	if(schemeForDocs == null){
    		schemeForDocs = schemeForDocsDefault;
    	}
    	if(hostForDocs == null){
    		hostForDocs = hostForDocsDefault;
    	}
    	if(portForDocs == null){
    		portForDocs = portForDocsDefault;
    	}
    }
    
    
    
    /**
     * This method is for child classes to override if any custom setup is to be done
     * before parents setup is called
     */
    public void beforeSetUp(){
    	port = environment.getProperty("local.server.port");
    	serverPath = UtilityGeneral.concat("http://localhost:",port);
    	
    }
    public String getFullUrl(String documentPathUrl){
    	return UtilityGeneral.concat("http://localhost:",port,documentPathUrl);
    }
    
    /**
     * This method is for child classes to override if any custom setup is to be done
     * after parents setup is called
     */
    public void afterSetUp() throws Exception{
    	
    	
    }
    
    @Before
    public void setUp() throws Exception {
    	beforeSetUp();
    	customisation();
    	createMockMvcInstance();
    	afterSetUp();
        
    }
    
    
    
//    private void createMockMvcInstance(){
//    	this.mockMvc = MockMvcBuilders
//                .webAppContextSetup(context)
////                .addFilters(springSecurityFilterChain)
////                .addFilters(traceFilter)
//                .alwaysDo(prepareJackson(objectMapper))
//                .alwaysDo(commonDocumentation())
//                .apply(documentationConfiguration(restDocumentation)
//                        .uris()
//                        .withScheme(schemeForDocs)
//                        .withHost(hostForDocs)
//                        .withPort(portForDocs)
//                        .and().snippets()
//                        .withDefaults(	
//                        				curlRequest()
//                        				,httpRequest()
//                        				,httpResponse()
//                        				,requestFields()
//                        				,responseFields()
//                        				,pathParameters()
//                        				,requestParameters()
//                        				,description()
//                        				,methodAndPath()
//                        				,section()
//                        				,authorization(DEFAULT_AUTHORIZATION)
//                        		     )
//                       )
//                
//                /*
//                 .withDefaults( authorization(DEFAULT_AUTHORIZATION)))
//                 */
//                .build();
//    	
//    	
//    	//create mockMvcWithoutDoc
////    	mockMvcWithoutDoc = MockMvcBuilders
////                .webAppContextSetup(context)
////              .alwaysDo(prepareJackson(objectMapper))
////              .build();
//    }
    private void createMockMvcInstance(){
    	DefaultMockMvcBuilder defaultMockMvcBuilder  = MockMvcBuilders
                .webAppContextSetup(context)
//                .addFilters(springSecurityFilterChain)
//                .addFilters(traceFilter)
                .alwaysDo(prepareJackson(objectMapper));
           
		defaultMockMvcBuilder.apply(documentationConfiguration(restDocumentation)
            .uris()
            .withScheme(schemeForDocs)
            .withHost(hostForDocs)
            .withPort(portForDocs)
            .and().snippets()
            .withDefaults(	
            				curlRequest()
            				,httpRequest()
            				,httpResponse()
            				,requestFields()
            				,responseFields()
            				,pathParameters()
            				,requestParameters()
            				,description()
            				,methodAndPath()
            				,section()
            				,authorization(DEFAULT_AUTHORIZATION)
            		     )
           );                
               
        this.mockMvc = defaultMockMvcBuilder.build();

    }
    

    private RestDocumentationResultHandler commonDocumentation() {
        return document("{class-name}/{method-name}",
                preprocessRequest(), commonResponsePreprocessor());
    }

    protected OperationResponsePreprocessor commonResponsePreprocessor() {
        return preprocessResponse(replaceBinaryContent(), limitJsonArrayLength(objectMapper),
                prettyPrint());
    }
    
    protected void prepareDocIfRequired(boolean isDocRequired, ResultActions resultActions) throws Exception{
    	if(isDocRequired){
    		commonDocumentation().handle(resultActions.andReturn());
    	}
    }
    protected void prepareDoc(ResultActions resultActions) throws Exception{
    	prepareDocIfRequired(true, resultActions);
    }
}
