package com.ssi.crossproject.common.utils;

import java.math.BigDecimal;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.text.ParseException;
import java.util.Currency;
import java.util.Iterator;
import java.util.Locale;
import java.util.Set;

import org.apache.commons.lang3.math.NumberUtils;
import org.springframework.format.number.NumberFormatter;
import org.springframework.format.number.NumberFormatAnnotationFormatterFactory;
import org.springframework.stereotype.Service;

@Service
public class UtilityNumber {
//	public String getFomat(String strNumber){
//		DecimalFormatSymbols.getInstance().
//		String identifiedFormat = null;
//		Number number = null;
//		NumberFormatter nf = new NumberFormatter("");
//		nf.setPattern("");
//		number = nf.getNumberFormat(java.util.Locale.US).parse(strNumber)
//		
//				java.util.Locale.US
//		return identifiedFormat;
//	}
//	public Number getFomatAsPerPattern(Locale local, String pattern, String strNumber) throws ParseException{
//		NumberUtils.isNumber(strNumber);
//		NumberFormatter nf = new NumberFormatter(pattern);
//		Number n =  nf.getNumberFormat(local).parse(strNumber);
//		nf = null;
//		return n;
//	}
	
	public static String getString(double val){
		return new BigDecimal(val).toPlainString();
	}
	public static String getNumerPart(String strInput){
		if(strInput != null ){
			strInput = strInput.trim();
			if(strInput.length()>0){
				String firstChar = strInput.substring(0,1);
				String truncatedInput = strInput;
				if(!NumberUtils.isDigits(firstChar)){					
					String percent = "%";
					if(strInput.startsWith(percent)){
						truncatedInput = strInput.replaceFirst(percent, "");
					}
					else{
						Set<Currency> stCurrecncy = Currency.getAvailableCurrencies();
						if(stCurrecncy != null && stCurrecncy.size()>0){
							Iterator<Currency> iterator = stCurrecncy.iterator();
							while(iterator.hasNext()){
								Currency curr = iterator.next();
								String symbol = curr.getSymbol();
								if(strInput.startsWith(symbol)){
									truncatedInput = strInput.substring(strInput.indexOf(symbol)+symbol.length());
									break;
								}
							}
						}
					}
				}
				try{
					truncatedInput = truncatedInput.replace(",","");
					return truncatedInput;
				}
				catch(Exception e1){					
					return null;
				}
			}			
		}
		return null;
	}
	
	public static String getNumericPart(String data){		
		if(data != null){
			StringBuilder sb = new StringBuilder();
			for(int i=0; i<data.length(); i++){
				char c = data.charAt(i);
				if(Character.isDigit(c)){
					sb.append(c);
				}
			}
			return sb.toString();
		}
		else{
			return "";
		}
	}

	public static Double getNumerFromCleanString(String strInput) {
//		String truncatedInput = getNumerPart(strInput);
		if (strInput != null) {
			try {
				return Double.valueOf(strInput);
			} catch (Exception e1) {
				try {
					return NumberUtils.createDouble(strInput)
							.doubleValue();
				} catch (Exception e2) {
					return null;
				}
			}
		}
		return null;
	}
	public static BigDecimal getBigDecimalFromCleanString(String strInput) {
//		String truncatedInput = getNumerPart(strInput);
		if (strInput != null) {
			try {
				return new BigDecimal(strInput);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		}
		return null;
	}

	public static  boolean isNumericString(String strInput){
		String truncatedInput = getNumerPart(strInput);
		Double val = getNumerFromCleanString(truncatedInput);
		if(val != null){
			return true;
		}
		return false;
	}
	
//	public static void main(String[] arg){
//		String input = ",\"0.000000\",\"0.000000\",\"0.000000\",\"0.000000\"";
//		String[] arr = getValues(input);
//		System.out.println(arr);
//	}
	
	public static  String[] getValues(String line){
		String[] arr = null;
		if(line != null){
			arr = line.split(",");
			if(arr != null){
				for(int i=0; i<arr.length; i++){
					String temp = arr[i];
					if(temp != null){
						temp = temp.trim();
						if(temp.startsWith("\"")){
							temp = temp.replaceFirst("\"", "");
						}
						if(temp.endsWith("\"")){
							if(temp.length() == 1){
								temp = "";
							}
							else{
								temp = temp.substring(0, temp.length()-2);
							}
						}
						arr[i] = temp;
					}
					
				}
			}
		}
		return arr;
	}
	
	
	
}
