package com.ssi.crossproject.common.utils;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import java.lang.reflect.Type;
import java.text.DateFormat;

import org.springframework.stereotype.Service;
@Service
public class UtilityJson {
	private static Gson gson;
    private static Gson gsonPrettyPrint;
    public static final String DATE_FORMAT = "dd/MM/yyyy HH:mm:ss zzz";
    private static Gson getGson(){
        if(gson == null){
            gson = new GsonBuilder().setDateFormat(DATE_FORMAT).create();
        }
        return gson;
    }

    private static Gson getGsonPrettyPrint(){
        if(gsonPrettyPrint == null){
            gsonPrettyPrint = new GsonBuilder().setDateFormat(DATE_FORMAT).setPrettyPrinting().create();
        }
        return gsonPrettyPrint;
    }

    /*
    Type listType = new TypeToken<List<String>>() {}.getType();
     */
    public static String getJsonString(Object obj, Type typeOfSrc){
        return getGson().toJson(obj, typeOfSrc);
    }

    /*
    Type listType = new TypeToken<List<String>>() {}.getType();
     */
    public static String getPrintableJsonString(Object obj, Type typeOfSrc){
        return getGsonPrettyPrint().toJson(obj, typeOfSrc);
    }
    public static String getPrintableJsonString(String jsonString, Type typeOfSrc){
        Object obj =  getObjectFromJsonString(jsonString, typeOfSrc);
        return getPrintableJsonString(obj, typeOfSrc);
    }

    public static Object getObjectFromJsonString(String strObj, Type typeOfSrc){
        return getGson().fromJson(strObj, typeOfSrc);
    }
}
